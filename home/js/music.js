// JavaScript Document
//定义歌曲信息数据
var hanser=new Object();
hanser=[
    {
        idx:1,
        name:"东京夏日相会",
        author:"YUKIri Hanser",
        imgUrl:"img/music/1.jpg",
        musicUrl:"music/1.mp3"
    },
    {
        idx:2,
        name:"明日夜空巡逻班",
        author:"Hanser",
        imgUrl:"img/music/2.jpg",
        musicUrl:"music/2.mp3"
    },
    {
        idx:3,
        name:"勾指起誓",
        author:"Hanser",
        imgUrl:"img/music/3.jpg",
        musicUrl:"music/3.mp3"
    },
    {
        id:4,
        name:"伴宅日记",
        author:"Hanser",
        imgUrl:"img/music/4.jpg",
        musicUrl:"music/4.mp3"
    },
    {
        id:5,
        name:"病名为爱",
        author:"YUKIri Hanser",
        imgUrl:"img/music/5.jpg",
        musicUrl:"music/5.mp3"
    },
    {
        idx:6,
        name:"平行线",
        author:"Hanser Syepias",
        imgUrl:"img/music/6.jpg",
        musicUrl:"music/6.mp3"
    },
    {
        idx:7,
        name:"吃货也想谈恋爱",
        author:"Hanser",
        imgUrl:"img/music/7.jpg",
        musicUrl:"music/7.mp3"
    },
    {
        idx:8,
        name:"小埋op",
        author:"Hanser",
        imgUrl:"img/music/8.jpg",
        musicUrl:"music/8.mp3"
    },
    {
        idx:9,
        name:"猫耳开关",
        author:"Hanser",
        imgUrl:"img/music/9.jpg",
        musicUrl:"music/9.mp3"
    },
    {
        idx:10,
        name:"打上花火",
        author:"YUKIri Hanser",
        imgUrl:"img/music/10.jpg",
        musicUrl:"music/10.mp3"
    },
    {
        idx:11,
        name:"拼凑的断音",
        author:"Hanser",
        imgUrl:"img/music/11.jpg",
        musicUrl:"music/11.mp3"
    },
    {
        idx:12,
        name:"东京冬日相会",
        author:"YUKIri Hanser",
        imgUrl:"img/music/12.jpg",
        musicUrl:"music/12.mp3"
    },
    {
        idx:13,
        name:"朝汐",
        author:"双笙 泠鸢yousa",
        imgUrl:"img/music/13.jpg",
        musicUrl:"music/13.mp3"
    },
    {
        idx:14,
        name:"一封孤岛的信",
        author:"YUKIri",
        imgUrl:"img/music/14.jpg",
        musicUrl:"music/14.mp3"
    },
    {
        idx:15,
        name:"夜空中最亮的星",
        author:"周玥",
        imgUrl:"img/music/15.jpg",
        musicUrl:"music/15.mp3"
    },
    {
        idx:16,
        name:"兔子先生",
        author:"伦桑",
        imgUrl:"img/music/16.jpg",
        musicUrl:"music/16.mp3"
    },
    {
        idx:17,
        name:"化生孤岛的鲸",
        author:"不才",
        imgUrl:"img/music/17.jpg",
        musicUrl:"music/17.mp3"
    },
    {
        idx:18,
        name:"起风了",
        author:"买辣椒也用券",
        imgUrl:"img/music/18.jpg",
        musicUrl:"music/18.mp3"
    },
    {
        idx:19,
        name:"那年十四",
        author:"池年",
        imgUrl:"img/music/19.jpg",
        musicUrl:"music/19.mp3"
    },
    {
        idx:20,
        name:"My Soul, Your Beats!",
        author:"Key Sounds Label ",
        imgUrl:"img/music/20.jpg",
        musicUrl:"music/20.mp3"
    },
    {
        idx:21,
        name:"逍遥叹",
        author:"徐薇",
        imgUrl:"img/music/21.jpg",
        musicUrl:"music/21.mp3"
    },
    {
        idx:22,
        name:"君の文字",
        author:"熊木杏里",
        imgUrl:"img/music/22.jpg",
        musicUrl:"music/22.mp3"
    },
    {
        idx:23,
        name:" 天ノ弱",
        author:"Akie秋绘",
        imgUrl:"img/music/23.png",
        musicUrl:"music/23.mp3"
    },
    {
        idx:24,
        name:"无心",
        author:"小缘",
        imgUrl:"img/music/24.jpg",
        musicUrl:"music/24.mp3"
    },
    {
        idx:25,
        name:"有何不可",
        author:"许嵩",
        imgUrl:"img/music/25.jpg",
        musicUrl:"music/25.mp3"
    },
    {
        idx:26,
        name:"如果当时",
        author:"许嵩",
        imgUrl:"img/music/25.jpg",
        musicUrl:"music/26.mp3"
    },
    {
        idx:27,
        name:"山水之间",
        author:"许嵩",
        imgUrl:"img/music/27.jpg",
        musicUrl:"music/27.mp3"
    },
    {
        idx:28,
        name:"清明雨上",
        author:"许嵩",
        imgUrl:"img/music/25.jpg",
        musicUrl:"music/28.mp3"
    },
    {
        idx:29,
        name:"庐州月",
        author:"许嵩",
        imgUrl:"img/music/29.jpg",
        musicUrl:"music/29.mp3"
    },
    {
        idx:30,
        name:"双人旁边",
        author:"许嵩",
        imgUrl:"img/music/31.jpg",
        musicUrl:"music/30.mp3"
    },
    {
        idx:31,
        name:"想象之中",
        author:"许嵩",
        imgUrl:"img/music/31.jpg",
        musicUrl:"music/31.mp3"
    },
    {
        idx:32,
        name:"玫瑰花的葬礼",
        author:"许嵩",
        imgUrl:"img/music/32.jpg",
        musicUrl:"music/32.mp3"
    },
    {
        idx:33,
        name:"断桥残雪",
        author:"许嵩",
        imgUrl:"img/music/33.jpg",
        musicUrl:"music/33.mp3"
    },
    {
        idx:34,
        name:"浅唱",
        author:"许嵩",
        imgUrl:"img/music/32.jpg",
        musicUrl:"music/34.mp3"
    },
    {
        idx:35,
        name:"星座书上",
        author:"许嵩",
        imgUrl:"img/music/25.jpg",
        musicUrl:"music/35.mp3"
    },
    {
        idx:36,
        name:"南山忆",
        author:"许嵩",
        imgUrl:"img/music/36.jpg",
        musicUrl:"music/36.mp3"
    }
];
//获取数据长度
var hanerlength=hanser.length;



//创建图标变量
var tubiao=document.createElement("i");
$(tubiao).addClass("fa fa-volume-up fa-lg");


//定义全局变量，用于控制歌单进程
var i=0; 

//渲染歌单列表并且绑定点击事件处理函数
var rendering=function(){
    for(k=0;k<hanerlength;k++){
        $(".list-group-item").eq(k).text(hanser[k].name).bind("click",function(){
            
            $(this).addClass("list-group-item-success");//设置颜色样式
            $(".list-group-item").not($(this)).removeClass("list-group-item-success");
            //获取当前点击元素的索引值
            idx=$(this).index(".list-group-item"); 
            //将索引值参数传递给全程变量用于控制歌单
            i=idx;
            //根据对应索引值改变图片文字音源路径
            musicName.innerHTML =hanser[idx].name;
            music_author.innerHTML=hanser[idx].author;
            audio.src=hanser[idx].musicUrl;
            music_mainimg.src=hanser[idx].imgUrl;
            if(audio.paused){
                audio.play(	);
                zimg.src="img/music/暂停.png";
                img.style.animationPlayState="running";
                $(".music_imgboxSide").addClass("play");
        
            }	
            else if(audio.played){
                audio.pause();
                zimg.src="img/music/播放.png";
                img.style.animationPlayState="paused";
                $(".music_imgboxSide").removeClass("play");
            }
            $(this).prepend(tubiao);
        });
    }
}

//调用渲染函数
rendering();

//为第一个歌单加上图标
$(".list-group-item").eq(0).prepend(tubiao);

// 暂停与播放
var img=document.getElementById("musicImg");                 //获取音乐唱片图片
var music_mainimg=document.getElementById("music_mainimg"); //获取音乐唱片主要的图片
var zimg=document.getElementById("play");                  //播放按钮图片
var aud=document.getElementById("audio");                 //音源控件
var musicName=document.getElementById("musicName");       //获取歌曲名字
var music_author=document.getElementById("music_author"); //获取歌手名字

//暂停与播放事件
play.onclick = function kai(){
	if(audio.paused){
		audio.play(	);
        zimg.src="img/music/暂停.png";
		img.style.animationPlayState="running";
		$(".music_imgboxSide").addClass("play");

	}	
	else if(audio.played){
		audio.pause();
        zimg.src="img/music/播放.png";
		img.style.animationPlayState="paused";
		$(".music_imgboxSide").removeClass("play");
	}
}


//实现按钮与音源的按钮同步
aud.onplay=function(){
	img.style.animationPlayState="running";
	zimg.src="img/music/暂停.png";
    $(".music_imgboxSide").addClass("play");
}
aud.onpause=function(){
	img.style.animationPlayState="paused";
	zimg.src="img/music/播放.png";
    $(".music_imgboxSide").removeClass("play");
}

//定义歌单显示函数
var gedan=function(i){
    if(i<12){
        $(".gedan").eq(0).slideDown();
        $(".gedan").eq(1).slideUp();
        $(".gedan").eq(2).slideUp();
    }
    else if(12<=i&&i<24){
        $(".gedan").eq(1).slideDown();
        $(".gedan").eq(0).slideUp();
        $(".gedan").eq(2).slideUp();
    }
    else{
        $(".gedan").eq(2).slideDown();
        $(".gedan").eq(0).slideUp();
        $(".gedan").eq(1).slideUp();
    }
}

//初始化变量
// 上一首事件
var prev1=document.getElementById("prev");
prev1.addEventListener("click",pre);
//定义上一首函数
var preloop=function(i){
	audio.src=hanser[i].musicUrl;
    aud.setAttribute("autoplay","ture");        //上一首为标签增加新的自动播放标签
    music_mainimg.src=hanser[i].imgUrl;       //更换图像路径
    musicName.innerHTML=hanser[i].name;      //更换歌名
    music_author.innerHTML=hanser[i].author;
    $(".music_imgboxSide").addClass("play");    //添加自动播放
    zimg.src="img/music/播放.png";              //同时控播放制按钮图片
    //绑定歌单处理为下一首激活样式和添加图标
    $(".list-group-item").eq(i).addClass("list-group-item-success");
    $(".list-group-item").not($(".list-group-item").eq(i)).removeClass("list-group-item-success");
    $(".list-group-item").eq(i).prepend(tubiao);
    gedan(i);
    
}
//上一首函数
function pre(){
	if(i>0)
	{
	i--;
	preloop(i);
	}
	else if(i==0){
    i=hanerlength-1;
	preloop(i);
	}
}

// 下一首事件
var Next=document.getElementById("next");
Next.addEventListener("click", nex);
//定义下一首函数
var nexloop=function(i){
	audio.src=hanser[i].musicUrl;
    aud.setAttribute("autoplay","ture");
    music_mainimg.src=hanser[i].imgUrl;
    musicName.innerHTML =hanser[i].name;
    music_author.innerHTML=hanser[i].author;
    $(".music_imgboxSide").addClass("play");
    zimg.src="img/music/播放.png";
    $(".list-group-item").eq(i).addClass("list-group-item-success");
    $(".list-group-item").not($(".list-group-item").eq(i)).removeClass("list-group-item-success");
    $(".list-group-item").eq(i).prepend(tubiao);
    gedan(i);

}
//下一首函数
function nex(){
	if(i<hanerlength-1)
	{
	i++;
	nexloop(i);
	}
	else if(i==hanerlength-1){
	i=0;
	nexloop(i);
	}
	
}

//循环事件
var xun=document.getElementById("xun");
xun.addEventListener("click", xh);
//控制是否循环播放
var xun=document.getElementById("xun")
function xh(){
	 if(aud.hasAttribute("loop"))//判断元素是否存在，如果存在是移除循环属性
      {
	  aud.removeAttribute("loop");
	  xun.src="img/music/顺序.png";

	  }
	  else
	{
     aud.setAttribute("loop","ture");//若元素不存在则添加循环属性
	  xun.src="img/music/循环.png";
	}
	

	
}

//自动播放事件
audio.addEventListener("ended",nex,function(){
	if(i==10){
		i=1;
		}
});



//歌单
//折叠效果
$(".gedan").hide().first().show();//让第一个显示其他消失
$(".panel-title ").click(function(){
    $(this).parent().next().slideDown(); //点击的元素的下一个子元素显示
    $(".gedan").not($(this).parent().next()).slideUp();
})


