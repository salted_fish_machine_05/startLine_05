;(function($, window){
    $.extend({
        //设置cookie
        addCookie: function(key,value,day,path,domain){
            //1.处理默认保存路径
            var index = window.location.pathname.lastIndexOf("/");
            var currentPath = window.location.pathname.slice(0,index);
            path = path || currentPath;
            //2.处理保存的domain
            domain = domain || document.domain;
            //3.处理默认时间
            if(!day){
                document.cookie=key+"="+value+";path="+path+";domain="+domain+";";
            }
            else{
                var date=new Date();
                date.setDate(date.getDate() + day);
                document.cookie=key+"="+value+";expires="+date.toGMTString()+";path="+path+";domain="+domain+";";
            }
        },
        //获取cookie的方法
        getCookie:function (key) {
            var res = document.cookie.split(";");//split字符串分割成数组的函数,获取到；前面变成一个数组
            for(var i=0;i<res.length;i++){
                var temp = res[i].split("=");
                if(temp[0].trim()===key){
                    return temp[1];
                }
            }
        },
        //删除cookie的方法
        delCookie:function (key,path) {  //path为设置保存的路径,默认删除默认保存路径("/"根路径)，如果要删除特定的路径，要path指明。注意之前的变成方法，要添加$符号调用
            $.addCookie(key,$.getCookie(key),-1,path);
        }
    });

})(jQuery,window);
